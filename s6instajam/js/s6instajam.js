var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var S6InstaJam;
(function (S6InstaJam) {
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main() {
            var _this = _super.call(this, 607, 1080, Phaser.AUTO, 'content', null) || this;
            _this.state.add('BootState', S6InstaJam.BootState, false);
            _this.state.add('PreloaderState', S6InstaJam.PreloaderState, false);
            _this.state.add('MainMenuState', S6InstaJam.MainMenuState, false);
            _this.state.add('GameState', S6InstaJam.GameState, false);
            _this.state.start('BootState');
            return _this;
        }
        return Main;
    }(Phaser.Game));
    S6InstaJam.Main = Main;
})(S6InstaJam || (S6InstaJam = {}));
window.onload = function () {
    var game = new S6InstaJam.Main();
};
var S6InstaJam;
(function (S6InstaJam) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(p_game, p_x, p_y) {
            var _this = _super.call(this, p_game, p_x, p_y, 'PlayerAtlas', 0) || this;
            _this.anchor.setTo(0.5, 0.5);
            p_game.add.existing(_this);
            _this.animations.add('idle', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 30, true).play();
            return _this;
        }
        return Player;
    }(Phaser.Sprite));
    S6InstaJam.Player = Player;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var Enemy = (function (_super) {
        __extends(Enemy, _super);
        function Enemy(p_game, p_player, p_x, p_y) {
            var _this = _super.call(this, p_game, p_x, p_y, 'ZombieAtlas1', 'zombie1_front_0.png') || this;
            _this.player = p_player;
            _this.anchor.setTo(0.5, 0.5);
            _this.exists = false;
            p_game.add.existing(_this);
            return _this;
        }
        Enemy.prototype.spawn = function (p_x, p_y) {
            this.reset(p_x, p_y);
            this.speed = 100;
        };
        Enemy.prototype.die = function () {
            this.kill();
        };
        Enemy.prototype.update = function () {
            if (this.exists) {
                this.game.physics.arcade.moveToXY(this, this.player.position.x, this.player.position.y, this.speed);
                if (this.game.physics.arcade.distanceBetween(this, this.player, true) <= 10.0) {
                    this.die();
                }
            }
        };
        return Enemy;
    }(Phaser.Sprite));
    S6InstaJam.Enemy = Enemy;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var EnemyPool = (function (_super) {
        __extends(EnemyPool, _super);
        function EnemyPool(p_game, p_player) {
            var _this = _super.call(this, p_game, p_game.world, 'EnemyPool', false, true, Phaser.Physics.ARCADE) || this;
            _this.player = p_player;
            _this.spawnRadius = p_game.world.height * 0.55;
            var offsetX = p_player.position.x + _this.spawnRadius;
            var offsetY = p_player.position.y + _this.spawnRadius;
            for (var count = 0; count < 100; ++count) {
                _this.add(new S6InstaJam.Enemy(_this.game, _this.player, offsetX, offsetY), true);
            }
            _this.setup();
            p_game.add.existing(_this);
            return _this;
        }
        EnemyPool.prototype.setup = function () {
            this.spawnRate = Phaser.Timer.SECOND * 0.5;
            this.nextSpawnTime = 0;
        };
        EnemyPool.prototype.spawn = function () {
            if (this.nextSpawnTime > this.game.time.time) {
                return;
            }
            var angle = Math.random() * 360;
            var positionX = this.player.position.x + this.spawnRadius * Math.cos(angle);
            var positionY = this.player.position.y + this.spawnRadius * Math.sin(angle);
            var enemy = this.getFirstExists(false);
            if (enemy == null) {
                return;
            }
            enemy.spawn(positionX, positionY);
            this.nextSpawnTime = this.game.time.time + this.spawnRate;
        };
        return EnemyPool;
    }(Phaser.Group));
    S6InstaJam.EnemyPool = EnemyPool;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        BootState.prototype.preload = function () {
            this.game.load.image('TitleBG', 'assets/img/title_nazi.png');
        };
        BootState.prototype.create = function () {
            this.game.stage.backgroundColor = '#ACE56E';
            this.input.maxPointers = 2;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.state.start('PreloaderState', true, false);
        };
        return BootState;
    }(Phaser.State));
    S6InstaJam.BootState = BootState;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var GameState = (function (_super) {
        __extends(GameState, _super);
        function GameState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        GameState.prototype.create = function () {
            this.player = new S6InstaJam.Player(this.game, this.game.world.centerX, this.game.world.centerY);
            this.enemies = new S6InstaJam.EnemyPool(this.game, this.player);
            this.weapon = new S6InstaJam.Weapon(this.game, this.player, this.enemies);
            this.isTrapPlanted = false;
            this.game.input.onDown.add(this.standbyWeapon, this);
            this.game.input.onUp.add(this.fire, this);
        };
        GameState.prototype.standbyWeapon = function (p_pointer) {
            if (this.isWorldRotating()) {
                return;
            }
            this.trapTimerEvent = this.game.time.events.add(Phaser.Timer.SECOND * 0.5, this.plantTrap, this);
        };
        GameState.prototype.fire = function (p_pointer) {
            if (p_pointer.identifier === null) {
                return;
            }
            if (this.isWorldRotating()) {
                return;
            }
            if (!this.isTrapPlanted) {
                this.weapon.fire();
            }
            this.isTrapPlanted = false;
            this.game.time.events.remove(this.trapTimerEvent);
        };
        GameState.prototype.plantTrap = function () {
            this.isTrapPlanted = true;
            console.log('plant trap');
        };
        GameState.prototype.isWorldRotating = function () {
            var isWorldRotating = this.game.input.keyboard.isDown(Phaser.KeyCode.SHIFT);
            isWorldRotating = isWorldRotating || (this.game.input.countActivePointers() > 1);
            return isWorldRotating;
        };
        GameState.prototype.update = function () {
            this.enemies.spawn();
        };
        return GameState;
    }(Phaser.State));
    S6InstaJam.GameState = GameState;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var MainMenuState = (function (_super) {
        __extends(MainMenuState, _super);
        function MainMenuState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MainMenuState.prototype.create = function () {
            this.titleBackground = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'TitleBG');
            this.titleBackground.anchor.setTo(0.5, 0.5);
            this.playButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY + (this.game.world.height * 0.4), 'UIAtlas', this.fadeOut, this, 'ui_btnPlayUp.png', 'ui_btnPlayUp.png', 'ui_btnPlayDown.png', 'ui_btnPlayUp.png');
            this.playButton.anchor.setTo(0.5, 0.5);
        };
        MainMenuState.prototype.fadeOut = function () {
            var tween = this.add.tween(this.playButton).to({ alpha: 0 }, 300, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);
        };
        MainMenuState.prototype.startGame = function () {
            this.game.state.start('GameState', true, false);
        };
        return MainMenuState;
    }(Phaser.State));
    S6InstaJam.MainMenuState = MainMenuState;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var PreloaderState = (function (_super) {
        __extends(PreloaderState, _super);
        function PreloaderState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        PreloaderState.prototype.preload = function () {
            this.titleBackground = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'TitleBG');
            this.titleBackground.anchor.setTo(0.5, 0.5);
            this.game.load.atlasJSONHash('BGAtlas', 'assets/atlas_img/bg.png', 'assets/atlas_json/bg.json');
            this.game.load.atlasJSONHash('UIAtlas', 'assets/atlas_img/ui.png', 'assets/atlas_json/ui.json');
            this.game.load.atlasJSONHash('FXAtlas', 'assets/atlas_img/fx.png', 'assets/atlas_json/fx.json');
            this.game.load.atlasJSONHash('GameObjectAtlas', 'assets/atlas_img/gobj.png', 'assets/atlas_json/gobj.json');
            this.game.load.atlasJSONHash('PlayerAtlas', 'assets/atlas_img/briffin_anim.png', 'assets/atlas_json/briffin_anim.json');
            this.game.load.atlasJSONHash('ZombieAtlas1', 'assets/atlas_img/zombie1_anim.png', 'assets/atlas_json/zombie1_anim.json');
            this.game.load.atlasJSONHash('ZombieAtlas2', 'assets/atlas_img/zombie2_anim.png', 'assets/atlas_json/zombie2_anim.json');
        };
        PreloaderState.prototype.create = function () {
            this.startMainMenu();
        };
        PreloaderState.prototype.startMainMenu = function () {
            this.game.state.start('MainMenuState', true, false);
        };
        return PreloaderState;
    }(Phaser.State));
    S6InstaJam.PreloaderState = PreloaderState;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var Bullet = (function (_super) {
        __extends(Bullet, _super);
        function Bullet(p_game) {
            var _this = _super.call(this, p_game, 0, 0, 'GameObjectAtlas', 'gobj_bulletFront.png') || this;
            _this.anchor.set(0.5);
            _this.exists = false;
            _this.speed = 500;
            _this.game.add.existing(_this);
            return _this;
        }
        Bullet.prototype.fire = function (p_x, p_y, p_target) {
            this.reset(p_x, p_y);
            this.target = p_target;
        };
        Bullet.prototype.update = function () {
            if (this.exists) {
                if (!this.target.exists) {
                    this.kill();
                }
                this.game.physics.arcade.moveToXY(this, this.target.position.x, this.target.position.y, this.speed);
                if (this.game.physics.arcade.collide(this, this.target)) {
                    this.kill();
                    this.target.die();
                }
            }
        };
        return Bullet;
    }(Phaser.Sprite));
    S6InstaJam.Bullet = Bullet;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var BulletPool = (function (_super) {
        __extends(BulletPool, _super);
        function BulletPool(p_game) {
            var _this = _super.call(this, p_game, p_game.world, 'BulletPool', false, true, Phaser.Physics.ARCADE) || this;
            for (var count = 0; count < 20; ++count) {
                _this.add(new S6InstaJam.Bullet(p_game), true);
            }
            p_game.add.existing(_this);
            return _this;
        }
        BulletPool.prototype.fire = function (p_x, p_y, p_target) {
            var bullet = this.getFirstExists(false);
            if (bullet === null) {
                return;
            }
            bullet.fire(p_x, p_y, p_target);
        };
        return BulletPool;
    }(Phaser.Group));
    S6InstaJam.BulletPool = BulletPool;
})(S6InstaJam || (S6InstaJam = {}));
var S6InstaJam;
(function (S6InstaJam) {
    var Weapon = (function (_super) {
        __extends(Weapon, _super);
        function Weapon(p_game, p_player, p_enemies) {
            var _this = _super.call(this, p_game, p_player.position.x, p_player.position.y) || this;
            _this.bulletPool = new S6InstaJam.BulletPool(p_game);
            _this.enemies = p_enemies;
            _this.quadTree = new Phaser.QuadTree(0, 0, p_game.world.width, p_game.world.height, 10, 4, 0);
            _this.quadTreeMarker = new Phaser.Rectangle(0, 0, 128, 128);
            _this.targetArray = [];
            _this.setup();
            p_game.add.existing(_this);
            return _this;
        }
        Weapon.prototype.setup = function () {
            this.fireRate = Phaser.Timer.SECOND * 0.1;
            this.nextFireTime = 0;
            this.range = 100;
            this.burstCapacity = 5;
        };
        Weapon.prototype.fire = function () {
            if (this.targetArray.length > 0) {
                return;
            }
            var existingEnemies = this.enemies.getAll('exists', true);
            var inputX = this.game.input.x;
            var inputY = this.game.input.y;
            var enemyOnCheck;
            if (existingEnemies.length <= 0) {
                return;
            }
            this.quadTree.clear();
            for (var idx = existingEnemies.length - 1; idx >= 0; --idx) {
                this.quadTree.insert(existingEnemies[idx]);
            }
            this.quadTreeMarker.x = inputX;
            this.quadTreeMarker.y = inputY;
            existingEnemies = this.quadTree.retrieve(this.quadTreeMarker);
            for (var idx = existingEnemies.length - 1; idx >= 0; --idx) {
                enemyOnCheck = existingEnemies[idx];
                if (this.game.physics.arcade.distanceToXY(enemyOnCheck, inputX, inputY) <= this.range) {
                    this.targetArray.push(enemyOnCheck);
                }
                if (this.targetArray.length >= this.burstCapacity) {
                    break;
                }
            }
        };
        Weapon.prototype.update = function () {
            if (this.targetArray.length <= 0) {
                return;
            }
            if (this.nextFireTime > this.game.time.time) {
                return;
            }
            this.bulletPool.fire(this.position.x, this.position.y, this.targetArray.pop());
            this.nextFireTime = this.game.time.time + this.fireRate;
        };
        return Weapon;
    }(Phaser.Sprite));
    S6InstaJam.Weapon = Weapon;
})(S6InstaJam || (S6InstaJam = {}));
